# Ryzentosh - ASRock B650M PG Riptide - Ryzen 9 7900

![Thumbnail](Docs/images/thumbnail.png)

# macOS Version
Current Version: macOS Sonoma - 14.3.1

# OpenCore Version
Current Version: 1.0.0 - Release

# Hardware
- Motherboard: ASRock B650M Riptide
- CPU: AMD Ryzen 9 7900
- RAM: 32 GB (2x16GB) Corsair Vengeance DDR5 6000Mhz CL30
- GPU: Sapphire NITRO+ RX 590 SE
- PSU: Corsair RM750e V2
- HDD: Crucial P3 Plus SSD 1TB
- WiFi/BT: Fenvi-T919 BCM4360KML

# Working
- CPU Name
- CPU Power Management
- GPU Management
- USB Ports
- Sleep/Wake
- WiFi/BT

# Not working
iGPU, but this is common. You should disable it in BIOS or via Device Properties just for macOS.

# BIOS Settings
You can use the default settings. But disable the iGPU. 

Appart from that, no change required. Above 4G Decording etc. are all already Enabled.

I also let Fast Boot Enabled, no issues so far.

Only if you need Secure Boot (Enable Secure Boot, Secure Boot Mode to Custom and then in the Key Management all .efi files in your EFI-folder need to be enrolled/whitelisted with "Enroll this EFI").

# USB Port Mapping

USB-Port Mapping is done via `SSDT-USB-B650M-Riptide.aml`:
- `XH00` is defined in `DSDT.aml`. 
- `XHC0`, `XHC1` and `XHC2` are defined in `SSDT-USB.aml`. 
- I had to fix all errors in `DSDT.aml` and removed USB-port definition from the `DSDT.aml`. 
- I also deleted the original `SSDT-USB.aml`. 
- The USB-Port Mapping for all controllers is in the `SSDT-USB-B650M-Riptide.aml`.

![USB-Port-Mapping_B650M-Riptide](Docs/images/usb-port-mapping_b650m-pg-riptide.png)

# Notes

- **iGPU is causing a crash on wake if enabled**. Disable iGPU in BIOS or disable it via Device Properties in your config.plist just for macOS
- **Two Keypresses to wake display after sleep** was solved by using `SSDT-USBW.aml` + `USBWakeFixup.kext` + Adding `acpi-wake-type=01` for every USB controller (`XH00`, `XHC0`, `XHC1` and `XHC2`) in the device properties.

# Credits

My work on this project is based on [SchmockLord](https://github.com/SchmockLord/Hackintosh-ASRock-B650M-Riptide-7800X3D) and [segunlee](https://github.com/segunlee/Hackintosh-ASRock-B650M-PG-Lightning) work, so big thanks to them, without them this project would not be possible.

- [SchmockLord](https://github.com/SchmockLord/Hackintosh-ASRock-B650M-Riptide-7800X3D)
- [segunlee](https://github.com/segunlee/Hackintosh-ASRock-B650M-PG-Lightning)
- [OpenCore](https://github.com/acidanthera/OpenCorePkg)
